module SalesforceDemodata

  class TaskCreator
    include CreatorCommon

    attr_reader :client, :parents

    def initialize(client, parent_sobjects)
      @client  = client
      @parents = parent_sobjects
    end

    def create!
      resp = client.bulkapi.create("Task", fake_tasks, true)
      BatchCreatorResponse.new resp
    end

    private

    def fake_tasks
      parents.each_with_object([]) do |sobject, tasks|
        rand(1...3).times { tasks << self.send("fake_#{sobject.sobject_type.downcase}_task", sobject) }
      end
    end

    def fake_task(time_min, what_id, subject)
      t =  forward_hours(time_min, rand(6) + 1)
      o = {
        Subject: subject,
        ActivityDate: forward_hours(t, rand(24)).to_date,
        WhatId: what_id,
        Status: task_status.sample,
        Priority: task_priorities.sample
      }
      set_audit_fields(o, created_date: t)
    end

    def fake_case_task(obj)
      fake_task(Time.parse(obj[:CreatedDate]), obj[:Id], "Working on #{obj[:Type]} case")
    end

    def fake_opportunity_task(obj)
      fake_task(Time.parse(obj[:CreatedDate]), obj[:Id], "Working on #{obj[:Name]} opportunity")
    end

    def task_status
      @task_status ||= client.api.picklist_values("Task", "Status").collect(&:value)
    end

    def task_priorities
      @task_priorities ||= client.api.picklist_values("Task", "Priority").collect(&:value)
    end

  end

end
