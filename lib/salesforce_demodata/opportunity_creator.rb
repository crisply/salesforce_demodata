module SalesforceDemodata

  class OpportunityCreator
    include CreatorCommon

    attr_reader :client

    def initialize(client, account_ids)
      @client = client
      @account_ids = account_ids
    end

    def create!(opportunity_percent: 0.70)
      aids = @account_ids.sample(@account_ids.length * opportunity_percent)
      opportunities = aids.each_with_object([]) do |aid, opps|
        rand(1...3).times { opps << fake_opportunity(aid) }
      end
      resp = client.bulkapi.create("Opportunity", opportunities, true)
      BatchCreatorResponse.new resp
    end


    private


    def fake_opportunity(account_id)
      o = {
        AccountId: account_id,
        Name: Faker::Commerce.product_name,
        CloseDate: Faker::Date.forward(rand(2...45)).iso8601,
        StageName: stage_names.sample,
        LeadSource: lead_sources.sample
      }
      o[:Amount] = Faker::Number.positive(10_000,250_000) if chance?
      set_audit_fields(o)
    end

    def stage_names
      @stage_names ||= client.api.picklist_values("Opportunity", "StageName").collect(&:value)
    end

    def lead_sources
      @lead_sources ||= client.api.picklist_values("Opportunity", "LeadSource").collect(&:value)
    end

  end
end
