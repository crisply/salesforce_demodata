module SalesforceDemodata
  module CreatorCommon

    CHANCE = [true, false]

    DEFAULT_DAYS_BACK = 14
    HOUR_SECONDS = 3600
    DAY_SECONDS = (24 * HOUR_SECONDS)

    private

    def activity_time(start_date = nil, end_date = nil)
      start_date ||= Faker::Time.backward(days_back + 1)
      end_date ||= Date.today.prev_day
      Faker::Time.between(start_date, end_date)
    end

    def activity_user
      client.available_user_ids.sample
    end

    def set_audit_fields(obj, **params)
      return obj unless client.audit_field_support?
      set_time_audit_fields(obj, created_date: params[:created_date], last_modified_date: params[:last_modified_date])
      set_user_audit_fields(obj, created_by_id: params[:created_by_id], last_modified_by_id: params[:last_modified_by_id])
    end

    def set_time_audit_fields(obj, created_date:, last_modified_date:)
      created_date ||= activity_time
      last_modified_date ||= (chance? ? activity_time(created_date) : created_date)
      obj[:CreatedDate] = created_date
      obj[:LastModifiedDate] = last_modified_date
      obj
    end

    def set_user_audit_fields(obj, created_by_id:, last_modified_by_id:)
      created_by_id ||= activity_user
      last_modified_by_id ||= (chance? ? activity_user : created_by_id)
      obj[:CreatedByID] = created_by_id
      obj[:LastModifiedbyID] = last_modified_by_id
      obj
    end

    def chance?
      CHANCE.sample
    end

    def days_back
      client.days_back rescue DEFAULT_DAYS_BACK
    end

    def days_ago(num_days)
      Time.now - (num_days * DAY_SECONDS)
    end

    def forward_hours(time, hours)
      time + (hours * HOUR_SECONDS)
    end

  end
end
