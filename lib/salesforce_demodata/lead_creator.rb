module SalesforceDemodata

  class LeadCreator
    include CreatorCommon

    attr_reader :client

    def initialize(client)
      @client = client
    end

    def create!(count:)
      leads = count.times.each_with_object([]) do |_, leads|
        leads << fake_lead
      end
      resp = client.bulkapi.create("Lead", leads, true)
      BatchCreatorResponse.new resp
    end


    private


    def fake_lead
      l = {
        FirstName: Faker::Name.first_name,
        LastName: Faker::Name.last_name,
        Company: lead_unique_name,
        Industry: lead_industries.sample,
        LeadSource: lead_sources.sample,
        Website: Faker::Internet.url,
        Rating: lead_ratings.sample,
      }
      l[:AnnualRevenue] = Faker::Number.positive(250_000,1_000_000_000) if chance?
      set_audit_fields(l)
    end

    def lead_unique_name
      @lead_names ||= Set.new
      begin
        name = Faker::Company.name
      end while( @lead_names.include? name )
      @lead_names << name
      name
    end

    def lead_industries
      @lead_industries ||= client.api.picklist_values("Lead", "Industry").collect(&:value)
    end

    def lead_ratings
      @lead_ratings ||= client.api.picklist_values("Lead", "Rating").collect(&:value)
    end

    def lead_sources
      @lead_sources ||= client.api.picklist_values("Lead", "LeadSource").collect(&:value)
    end

  end
end
