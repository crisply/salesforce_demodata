module SalesforceDemodata

  class AccountCreator
    include CreatorCommon

    attr_reader :client

    def initialize(client)
      @client = client
    end

    def create!(count:)
      accounts = count.times.each_with_object([]) do |_, accounts|
        accounts << fake_account
      end
      resp = client.bulkapi.create("Account", accounts, true)
      BatchCreatorResponse.new resp
    end


    private


    def fake_account
      a = {
        Name: account_unique_name,
        Type: account_types.sample,
        Industry: account_industries.sample,
        Website: Faker::Internet.url,
        Rating: account_ratings.sample,
      }
      a[:AnnualRevenue] = Faker::Number.positive(250_000,1_000_000_000) if chance?
      set_audit_fields(a)
    end

    def account_unique_name
      @account_names ||= Set.new
      begin
        name = Faker::Company.name
      end while( @account_names.include? name )
      @account_names << name
      name
    end

    def account_types
      @account_types ||= client.api.picklist_values("Account", "Type").collect(&:value)
    end

    def account_industries
      @account_industries ||= client.api.picklist_values("Account", "Industry").collect(&:value)
    end

    def account_ratings
      @account_ratings ||= client.api.picklist_values("Account", "Rating").collect(&:value)
    end

  end
end
