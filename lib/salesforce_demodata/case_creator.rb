module SalesforceDemodata

  class CaseCreator
    include CreatorCommon

    attr_reader :client

    def initialize(client, account_ids)
      @client = client
      @account_ids = account_ids
    end

    def create!(case_percent: 0.75)
      aids = @account_ids.sample(@account_ids.length * case_percent)
      cases = aids.each_with_object([]) do |aid, cases|
        rand(1...5).times { cases << fake_case(aid) }
      end
      resp = client.bulkapi.create("Case", cases, true)
      BatchCreatorResponse.new resp
    end


    private


    def fake_case(account_id)
      c = {
        AccountId: account_id,
        Origin: origins.sample,
        Status: statuses.sample,
        Type: types.sample,
      }
      set_audit_fields(c)
    end

    def origins
      @origins ||= client.api.picklist_values("Case", "Origin").collect(&:value)
    end

    def statuses
      @statuses ||= client.api.picklist_values("Case", "Status").collect(&:value)
    end

    def types
      @types ||= client.api.picklist_values("Case", "Type").collect(&:value)
    end

  end
end
