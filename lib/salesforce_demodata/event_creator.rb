module SalesforceDemodata

  class EventCreator
    include CreatorCommon

    USE_CASES = true
    USE_OPPORTUNITIES = true
    USE_CONTACTS = true
    USE_ACCOUNTS = true

    attr_reader :client, :parents

    def initialize(client, parent_sobjects)
      @client = client
      @parents = parent_sobjects
    end

    def create!
      events = parents.each_with_object([]) do |sobject, events|
        next unless using_type?(sobject.sobject_type)
        rand(1...3).times { events << self.send("fake_#{sobject.sobject_type.downcase}_event", sobject) }
      end
      resp = client.bulkapi.create("Event", events, true)
      BatchCreatorResponse.new resp
    end

    private


    def fake_event(time_min, what_id, who_id, subject)
      t = forward_hours(time_min, rand(6)+1)
      o = {
          Subject: subject,
          StartDateTime: t,
          DurationInMinutes: (rand(10) + 1) * 15
      }
      o[:WhatId] = what_id unless what_id.nil?
      o[:WhoId] = who_id unless who_id.nil?
      set_audit_fields(o, created_date: time_min, last_modified_date: time_min)
    end

    def fake_case_event(obj)
      fake_event(Time.parse(obj[:CreatedDate]), obj[:Id], nil, "Meeting about #{obj[:Type]} case")
    end

    def fake_opportunity_event(obj)
      fake_event(Time.parse(obj[:CreatedDate]), obj[:Id], nil, "Meeting about #{obj[:Name]} opportunity")
    end

    def fake_contact_event(obj)
      fake_event(Faker::Time.backward(days_back + 1), nil, obj[:Id], "Call with #{obj[:FirstName]} #{obj[:LastName]}")
    end

    def fake_account_event(obj)
      fake_event(Time.parse(obj[:CreatedDate]), obj[:Id], nil, "Conference Call with #{obj[:Name]}")
    end

    def using_type?(sobject_type)
      case sobject_type
      when "Account"
        USE_ACCOUNTS
      when "Case"
        USE_CASES
      when "Contact"
        USE_CONTACTS
      when "Opportunity"
        USE_OPPORTUNITIES
      else
        false
      end
    end

  end

end
