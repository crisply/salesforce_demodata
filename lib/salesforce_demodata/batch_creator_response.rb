module SalesforceDemodata
  class BatchCreatorResponse

    attr_reader :resp

    def initialize(resp)
      @resp = resp
    end

    def ids
      @ids ||= resp["batches"][0]["response"].each_with_object([]) do |b,all|
        all << b["id"][0] if b["success"][0] == "true"
      end
    end

  end
end
