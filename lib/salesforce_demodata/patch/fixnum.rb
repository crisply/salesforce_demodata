require "time"

class Fixnum

  def days
    self * (24 * ( 60 ** 2 ))
  end unless respond_to?(:days)

  def hours
    self * (60 ** 2)
  end unless respond_to?(:hours)

  def minutes
    self * 60
  end unless respond_to?(:minutes)

  def ago
    Time.now - self
  end unless respond_to?(:ago)

end
