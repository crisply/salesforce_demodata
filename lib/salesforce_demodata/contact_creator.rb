module SalesforceDemodata

  class ContactCreator
    include CreatorCommon

    attr_reader :client

    def initialize(client, account_ids)
      @client = client
      @account_ids = account_ids
    end

    def create!
      contacts = @account_ids.each_with_object([]) do |aid, contacts|
        rand(1..10).times { contacts << fake_contact(aid) }
      end
      resp = client.bulkapi.create("Contact", contacts, true)
      BatchCreatorResponse.new resp
    end


    private


    def fake_contact(account_id)
      first_name = Faker::Name.first_name
      last_name = Faker::Name.last_name
      email = Faker::Internet.safe_email("#{first_name} #{last_name}")
      c = {
        AccountId: account_id,
        FirstName: first_name,
        LastName: last_name,
        Email: email,
        Phone: Faker::PhoneNumber.phone_number,
        HomePhone: Faker::PhoneNumber.phone_number,
        MobilePhone: Faker::PhoneNumber.cell_phone,
      }
      set_audit_fields(c, created_date: days_ago(days_back))
    end

  end
end
