require "restforce"
require "salesforce_bulk_api"
require "faker"
require "set"
require "salesforce_demodata/patch/fixnum"
require "salesforce_demodata/version"

module SalesforceDemodata

  class Client

    DEFAULT_CLEAN_TYPES = %w{Lead Case Opportunity Task Event Contact Account}

    attr_reader :bulkapi, :api, :audit_field_support, :base_count, :days_back
    alias_method :audit_field_support?, :audit_field_support

    def initialize(audit_field_support: true, base_count: 200, days_back: 14, **restforce_args)
      @audit_field_support = audit_field_support
      @base_count = base_count
      @days_back = days_back
      @api = ::Restforce.new(restforce_args)
      @api.instance_url # this needs to be called before passing to SalesforceBulkApi
      @bulkapi = ::SalesforceBulkApi::Api.new @api
    end

    def available_account_ids(refresh: false)
      available_accounts(refresh: refresh).collect(&:Id)
    end

    def available_accounts(refresh: false)
      return @available_accounts if(defined?(@available_accounts) && !refresh)
      @available_accounts = api.query("Select Id, CreatedDate, Name From Account")
    end

    def available_cases(refresh: false)
      return @available_cases if(defined?(@available_cases) && !refresh)
      @available_cases = api.query("Select Id, CreatedDate, Type From Case")
    end

    def available_contacts(refresh: false)
      return @available_contacts if(defined?(@available_contacts) && !refresh)
      @available_contacts = api.query("Select Id, CreatedDate, FirstName, LastName From Contact")
    end

    def available_opportunities(refresh: false)
      return @available_opportunities if(defined?(@available_opportunities) && !refresh)
      @available_opportunities = api.query("Select Id, CreatedDate, Name From Opportunity")
    end

    def available_user_ids(refresh: false)
      available_users(refresh: refresh).collect(&:Id)
    end

    def available_users(refresh: false)
      return @available_users if(defined?(@available_users) && !refresh)
      @available_users = api.query("Select Id From User")
    end

    def unconverted_lead_ids
      api.query("Select Id From Lead Where ConvertedAccountId = null").collect(&:Id)
    end

    def convert_leads!(lead_ids:)
      lead_ids.each do |id|
        api.get "/services/apexrest/Lead/#{id}"
      end
      self
    end

    def create_all!
      puts "Creating Leads...."
      lead_ids = create_leads!
      puts "Converting some Leads...."
      convert_leads! lead_ids: lead_ids.sample((lead_ids.length * 0.75).to_i)
      puts "Creating Accounts...."
      account_ids = create_accounts!
      puts "Creating Contacts...."
      create_contacts! account_ids: account_ids
      puts "Creating Opportunities...."
      create_opportunities! account_ids: account_ids
      puts "Creating Cases...."
      create_cases! account_ids: account_ids
      puts "Creating Tasks..."
      create_tasks! parents: (available_cases.to_a + available_opportunities.to_a)
      puts "Creating Events..."
      create_events! parents: (available_cases.to_a +
                     available_opportunities.to_a +
                     available_accounts.to_a +
                     available_contacts.to_a)
      self
    end

    def create_accounts!(count: base_count)
      AccountCreator.new(self).create!(count: count).ids
    end

    def create_cases!(account_ids: available_account_ids)
      CaseCreator.new(self, account_ids).create!
    end

    def create_contacts!(account_ids: available_account_ids)
      ContactCreator.new(self, account_ids).create!
    end

    def create_leads!(count: base_count)
      LeadCreator.new(self).create!(count: count).ids
    end

    def create_opportunities!(account_ids: available_account_ids)
      OpportunityCreator.new(self, account_ids).create!
    end

    def create_tasks!(parents:)
      TaskCreator.new(self, parents).create!
    end

    def create_events!(parents:)
      EventCreator.new(self, parents).create!
    end

    # @params [Array<String>] types The Salesforce objects to bulk delete
    def delete_records!(types: DEFAULT_CLEAN_TYPES)
      types.each do |type|
        ids = api.query("Select Id From #{type}").collect{|o| {id: o.Id}}
        bulkapi.hard_delete(type, ids)
      end
      self
    end

  end

end

require "salesforce_demodata/creator_common"
require "salesforce_demodata/account_creator"
require "salesforce_demodata/case_creator"
require "salesforce_demodata/contact_creator"
require "salesforce_demodata/lead_creator"
require "salesforce_demodata/opportunity_creator"
require "salesforce_demodata/task_creator"
require "salesforce_demodata/event_creator"
require "salesforce_demodata/batch_creator_response"
