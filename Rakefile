require "bundler/gem_tasks"

BASEDIR=File.dirname(__FILE__)
TOOLING_JAR="https://github.com/neowit/tooling-force.com/releases/download/v0.3.3.2/tooling-force.com-0.3.3.2.jar"

desc "Open a Pry console for this library"
task :console do
  require "pry"
  require "salesforce_demodata"
  ARGV.clear
  Pry.start
end

namespace :salesforce do
  SFDIR   = "#{BASEDIR}/DemoDataApex"
  JARFILE = "#{SFDIR}/tooling-force.com.jar"

  desc "Download the tooling JAR"
  task :get_tool do
    require "open-uri"
    unless File.exist?(JARFILE)
      pbar = nil
      content_length_p = ->(len) { pbar = ProgressBar.new(len) if(len && len > 0) }
      progress_p       = ->(len) { pbar.update(len) }

      open(TOOLING_JAR, "r", content_length_proc: content_length_p, progress_proc: progress_p) do |io|
        File.open(JARFILE, "wb") {|f| f.write io.read }
      end
    end
  end

  desc "Deploy Salesforce assets"
  task :deploy do
    Rake::Task["salesforce:get_tool"].invoke
    propfile = "#{SFDIR}/build.properties"
    unless File.exist?(propfile)
      print "Username: "
      user = STDIN.gets.chomp
      print "Password: "
      pass = STDIN.gets.chomp
      print "Sectoken: "
      tok  = STDIN.gets.chomp
      File.open(propfile, "w") do |f|
        f.puts "sf.username = #{user}"
        f.puts "sf.password = #{pass}#{tok}"
        f.puts "sf.serverurl = https://login.salesforce.com"
      end
    end
    sh %{ java -jar #{JARFILE} --config=#{propfile} --action=deployAll --projectPath=#{SFDIR} --responseFilePath=#{SFDIR}/response.log }
  end
end

class ProgressBar
  attr_reader :content_length, :segment_size
  attr_accessor :current_bars, :spindex

  SPINNER = %w{- \\ | /}
  PSYM    = "."

  def initialize(content_length, bars = 50)
    @content_length = content_length
    @bars = bars
    @segment_size = (@content_length / @bars).to_f
    @current_bars = 0
    @spindex = 0
    puts "'#{PSYM}' = #{bars}"
  end

  def update(current_size)
    new_bars = (current_size / segment_size).ceil
    if new_bars > current_bars
      self.current_bars = new_bars
      print PSYM
    else
      spin
    end
  end

  private

  def spin
    print "#{SPINNER[spindex]}\b"
    self.spindex = (spindex + 1) % SPINNER.length
  end
end
