// http://stackoverflow.com/questions/19663119/is-there-any-rest-service-available-in-saleforce-to-convert-leads-into-accounts
@RestResource(urlMapping='/Lead/*')
global with sharing class RestLeadConvert {

global class RestLeadConvertResponse {
    public String label;
    public Boolean status;

    public RestLeadConvertResponse() {
        this.label = 'success';
        this.status = false;
    }
}

@HttpGet
global static RestLeadConvertResponse doGet() {
    RestLeadConvertResponse response = new RestLeadConvertResponse();
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    String leadId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
    Database.LeadConvert lc = new Database.LeadConvert();
    lc.setLeadId(leadId);

    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.setConvertedStatus(convertStatus.MasterLabel);
    Database.LeadConvertResult lcr ;
    try{
        lcr = Database.convertLead(lc);
        system.debug('*****lcr.isSuccess()'+lcr.isSuccess());
        response.status = true;
    }
    catch(exception ex){
        system.debug('***NOT CONVERTED**');
    }
    return response;
}
}
