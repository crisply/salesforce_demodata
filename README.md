# SalesforceDemodata

This gem aids in the creation and deletion of demo assets for Salesforce. See the **Usage** section below for a brief tutorial.

## Installation

```base
git checkout git@bitbucket.org:crisply/salesforce_demodata.git
cd salesforce_demodata
bundle
```

### Add a support case to turn on audit fields for your org

Here is an example ticket you can base your support request off of. Depending on
who does the request they may or may not let you keep access indefinitely.

```
Category: Technical Support
Topic: Data Management

Turn on audit fields for dev org

1. Org ID: <your org id>

2. Name of the feature or limit: Access to system audit fields
I have data imports for testing that I would like to override some of the audit
fields for such as CreatedDate. Can you please turn this on for this dev org?
Thank you.

3. Duration that the requested change should remain in effect:
This is something I need access to indefinitely. This is a development
environment that I load with test data and I need to do this on a continuing
basis.
```

### Install the Salesforce Assets

There is now a Rake task that automates most of the Salesforce installation. It will install the RestLeadConvert class
as well as the **Bulk API Admin** permission set. You should have your username, password and security token ready for input.

* Run `rake salesforce:deploy`
* Add users to the **Bulk API Admin** permission set.
    * In the Salesforce UI browse to **Setup** -> **Manage Users** -> **Permission Sets**
    * Click **Bulk API Admin**
    * Select **Manage Assignments** and add the user you want to have this permission.


## Usage

You can open a Pry console with the gem loaded with `rake console`.

* Create a Client Instance
    * `client = SalesforceDemodata::Client.new username: sflogin, password: sfpass+sectoken, client_id: client_id, client_secret: client_sec`
    * If you don't have writeable audit field support for your Org pass `audit_field_support: false`
* Create All Artifacts
    * `client.create_all!`
* Delete All Artifacts
    * `client.delete_records!`
