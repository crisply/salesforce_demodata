# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'salesforce_demodata/version'

Gem::Specification.new do |spec|
  spec.name          = "salesforce_demodata"
  spec.version       = SalesforceDemodata::VERSION
  spec.authors       = ["Dan Wanek"]
  spec.email         = ["dan@crisply.com"]
  spec.summary       = %q{Generates demo data for a Salesforce Org.}
  spec.description   = %q{Generates demo data for a Salesforce Org.}
  spec.homepage      = "https://bitbucket.org/crisply/salesforce_demodata"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "restforce"
  spec.add_dependency "salesforce_bulk_api"
  spec.add_dependency "faker"

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "pry"
end
